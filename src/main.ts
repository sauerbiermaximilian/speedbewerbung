import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
import App from "./App.vue";
import "./index.css";

import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import Admin from "./views/Admin.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", name: "Home", component: Home },
    { path: "/login", name: "Login", component: Login },
    { path: "/admin", name: "Admin", component: Admin },
  ],
});

createApp(App).use(router).mount("#app");
