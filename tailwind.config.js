module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        brand: {
          light: "#E45364",
          blue: "#283853",
          gray: "#f7f7f7",
        },
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
